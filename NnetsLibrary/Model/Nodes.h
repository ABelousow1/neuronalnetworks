#pragma once
#include "../types.h"
#include <vector>
#include <string>
using std::vector;
using std::string;


class AbstractNode {
	static int internal_ids;
	int internal_id;
	string name;
public:
	int getId() { return internal_id; }
	string getName() { return name; }
protected:
	//AbstractNode();
};


class SequentialNode: public AbstractNode
{
	NodeType type;
	int nodeId;
	SequentialNode* previousNode;
	SequentialNode* nextNode;
};


class NodeSequen�ce: public AbstractNode
{
	SequentialNode* input;
	SequentialNode* output;
};


class ManyOneConnector: AbstractNode {
	vector<NodeSequen�ce*> many;
	NodeSequen�ce* one;
};


class Splitter :ManyOneConnector {
	Splittype splitType;
};


class Merger :ManyOneConnector {
	Mergetype mergeType;
};


class FullyConnectedNode {
	int inputUnits, outputUnits;
	int bias;
	Weights* w;

};

