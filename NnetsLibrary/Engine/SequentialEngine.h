#pragma once
#include "../Model/Nodes.h"
class SequentialEngine
{
	bool isLearning;
public:
	SequentialEngine(SequentialNode *input, SequentialNode *output, bool learning) : isLearning(learning) {}
	void goForward();
	void goBackward();
	void processCurrentForward(SequentialNode* currentNode);
	void processCurrentBackward(SequentialNode* currentNode);
	void processFullyConnected(SequentialNode* currentNode);
	void processConvolution(SequentialNode* currentNode);
	void processRecursive(SequentialNode* currentNode);
	void saveIntermediateWeights(SequentialNode* currentNode);
};

