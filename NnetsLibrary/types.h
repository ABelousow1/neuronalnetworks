#pragma once
#include <vector>
enum class NodeType
{
	convolution, fullyconnected, recursive, usertype
};
enum class Splittype {
	copy, other
};
enum class Mergetype {
	merge, other
};

class Weights {
	int xd, yd;
	std::vector<double> data;
	Weights(int xd_=0, int yd_=0): xd(xd_),yd(yd_), data(xd*yd){}
	
};

using Data = std::vector<double>;