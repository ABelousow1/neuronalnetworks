#include "ActivationFunctions.h"
#include <algorithm>
#include <cmath>

double sigmoid(double x) 
{
	return 1 / (1 + exp(-x));
}

double ReLu(double x)
{
	if (x > 0) return x;
	else return 0;
}

double tahn(double x)
{
	return 2*sigmoid(2*x) - 1;
}

